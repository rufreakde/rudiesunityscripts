﻿/*********************
* Copyright (c) 2014, Rudolf Chrispens.  All rights reserved.
* Copyrights licensed under the GNU License.
* https://www.gnu.org/licenses/gpl-3.0
***********************/

#region USE
using UnityEngine;
using UnityEditor;
#endregion

[CustomPropertyDrawer(typeof(AutoAssignAttribute))]
[SerializeField]
public class AutoAssignDrawer : PropertyDrawer
{
    float buttonWidth   = 100f;
    AutoAssignAttribute autoAssignAttribute = null;

    public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
    {
        //button width
        EditorGUIUtility.labelWidth = EditorGUIUtility.labelWidth - buttonWidth;

        //get attribute
        autoAssignAttribute = (AutoAssignAttribute)base.attribute;

        if (autoAssignAttribute.Auto && _Property.objectReferenceValue == null)
        {
            //Custom Auto Button
            Rect pos1 = new Rect(_Position.x, _Position.y, buttonWidth, _Position.height);
            Rect pos2 = new Rect(_Position.x + buttonWidth, _Position.y, _Position.width - buttonWidth, _Position.height);
            //default assignable property
            if(GUI.Button(pos1, "Auto"))
            {
                autoAssignAttribute.Auto = false;
            }

            string propType = _Property.type.Remove(0, 5);
            EditorGUI.LabelField(pos2, _Property.displayName, propType.Remove(propType.Length-1));
        }
        else
        {
            Rect pos1 = new Rect (_Position.x, _Position.y, buttonWidth, _Position.height);
            Rect pos2 = new Rect (_Position.x + buttonWidth, _Position.y, _Position.width - buttonWidth, _Position.height);
            //default assignable property
            if (GUI.Button(pos1, "Manual"))
            {
                autoAssignAttribute.Auto = true;
                _Property.objectReferenceValue = null;
            }
            EditorGUI.PropertyField(pos2, _Property);
        }
    }
}