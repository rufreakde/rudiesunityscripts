﻿/*********************
* Copyright (c) 2014, Rudolf Chrispens.  All rights reserved.
* found on http://www.rudolf-chrispens.de/posts/
* Copyrights licensed under the GNU License.
* https://www.gnu.org/licenses/gpl-3.0
***********************/

#region USE
using UnityEngine;
using System;
#endregion

[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
public class AutoAssignAttribute : PropertyAttribute
{
    private bool auto = true;

    public bool Auto
    {
        get { return auto; }
        set { auto = value; }
    }
}
