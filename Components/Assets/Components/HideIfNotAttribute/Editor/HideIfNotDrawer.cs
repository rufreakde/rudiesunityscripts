﻿/*********************
* Copyright (c) 2014, Rudolf Chrispens.  All rights reserved.
* found on http://www.rudolf-chrispens.de/posts/
* Copyrights licensed under the GNU License.
* https://www.gnu.org/licenses/gpl-3.0
***********************/

using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(HideIfNotAttribute))]
public class HideIfNotDrawer : PropertyDrawer
{
    bool show = true;
    HideIfNotAttribute hideINAttribute;
    SerializedProperty refProperty;

    SerializedPropertyType referenceType;
    System.Type referencesType;
    System.Type chosenType;

    public override float GetPropertyHeight(SerializedProperty _Property, GUIContent _Label)
    {
        hideINAttribute = (HideIfNotAttribute)attribute;

        #region FALLBACK FOR ARRAYS
        refProperty = _Property.serializedObject.FindProperty(hideINAttribute.PropertyName);

        if (refProperty == null)
        {
            //if its not found it may be incapsulated
            refProperty = GetCapsulatedProperty(_Property, hideINAttribute);
        }
        #endregion

        if (refProperty != null)
        {
            referenceType = refProperty.propertyType;
            chosenType = hideINAttribute.RefType;

            //works with boolean / integer / string 

            referencesType = AssemblyExtension.GetTypeByAssembly(refProperty.type);
            Debug.Log(refProperty.type + "  =>  type: " + referencesType);

            if (chosenType == typeof(bool) && referenceType == SerializedPropertyType.Boolean)
                show = CheckBool(refProperty, hideINAttribute, _Label);

            else if (chosenType == typeof(int) && referenceType == SerializedPropertyType.Integer)
                show = CheckInt(refProperty, hideINAttribute, _Label);

            else if (chosenType == typeof(string) && referenceType == SerializedPropertyType.String)
                show = CheckString(refProperty, hideINAttribute, _Label);
            else
                Debug.LogError("Error: Attribut was not defined right!  ->  (" + referenceType + " " + hideINAttribute.PropertyName + ") != " + chosenType);
        }
        else
        {
            Debug.LogError("Error: Property not found!" + this.ToString());
            return base.GetPropertyHeight(_Property, _Label);
        }

        if (show)
        {
            return base.GetPropertyHeight(_Property, _Label);
        }
        else
            return 0f;
    }

    public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
    {
        System.Type tTypeCompare = typeof(bool);

        if (refProperty.propertyType == SerializedPropertyType.Boolean)
        {
            tTypeCompare = typeof(bool);
        }
        else if (refProperty.propertyType == SerializedPropertyType.Integer)
        {
            tTypeCompare = typeof(int);
        }
        else if (refProperty.propertyType == SerializedPropertyType.String)
        {
            tTypeCompare = typeof(string);
        }

        if (refProperty != null)
        {
            if (tTypeCompare == hideINAttribute.RefType)
            {
                if (show)
                    EditorGUI.PropertyField(_Position, _Property, _Label, true);
            }
            else
            {
                EditorGUI.HelpBox(_Position, string.Format("Property {0} is not of Type: (" + hideINAttribute.RefType.ToString() + ")", hideINAttribute.PropertyName), MessageType.Error);
            }
        }
        else
        {
            EditorGUI.HelpBox(_Position, string.Format("Couldn't find property {0}", hideINAttribute.PropertyName), MessageType.Error);
        }
    }

    public SerializedProperty GetCapsulatedProperty(SerializedProperty _PropertyOfAttribute, HideIfNotAttribute _Attribute)
    {
        SerializedProperty refPropertyHolder = _PropertyOfAttribute.serializedObject.FindProperty(_Attribute.PropHolder);

        if(refPropertyHolder == null)
        {
            return null;//didnt find an array
        }

        //return refProperty of that array ID
        for(int i=0; i< refPropertyHolder.arraySize; i++)
        {
            if (refPropertyHolder.GetArrayElementAtIndex(i).FindPropertyRelative(_Attribute.Name).propertyPath == _PropertyOfAttribute.propertyPath)
            {
                return refPropertyHolder.GetArrayElementAtIndex(i).FindPropertyRelative(_Attribute.PropertyName);
            }
        }

        return null; //didnt find the right property
    }

    public bool CheckBool(SerializedProperty _RefProp, HideIfNotAttribute _Attribute, GUIContent _label)
    {
        if (_Attribute.HideOnRefValue)
        {
            if (_RefProp != null && _RefProp.boolValue == _Attribute.RefBool)
            {
                return false;
            }
            else
                return true;
        }
        else
        {
            if (_RefProp != null && _RefProp.boolValue != _Attribute.RefBool)
            {
                return false;
            }
            else
                return true;
        }
    }

    public bool CheckInt(SerializedProperty _RefProp, HideIfNotAttribute _Attribute, GUIContent _label)
    {
        if (_Attribute.HideOnRefValue)
        {
            if (_RefProp != null && _RefProp.intValue == _Attribute.RefInt)
            {
                return false;
            }
            else
                return true;
        }
        else
        {
            if (_RefProp != null && _RefProp.intValue != _Attribute.RefInt)
            {
                return false;
            }
            else
                return true;
        }
    }

    public bool CheckString(SerializedProperty _RefProp, HideIfNotAttribute _Attribute, GUIContent _label)
    {
        if (_Attribute.HideOnRefValue)
        {
            if (_RefProp != null && _RefProp.stringValue == _Attribute.RefString)
            {
                return false;
            }
            else
                return true;
        }
        else
        {
            if (_RefProp != null && _RefProp.stringValue != _Attribute.RefString)
            {
                return false;
            }
            else
                return true;
        }
    }

}