﻿/*********************
* Copyright (c) 2014, Rudolf Chrispens.  All rights reserved.
* found on http://www.rudolf-chrispens.de/posts/
* Copyrights licensed under the GNU License.
* https://www.gnu.org/licenses/gpl-3.0
***********************/

using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

public static class AssemblyExtension 
{
    //this script is used for reflection and type recognition

    /// <summary>
    /// Get the default assembly of Unity
    /// </summary>
    /// <returns></returns>
    public static Assembly GetUnityEngineAssembly()
    {
        return typeof(UnityEngine.Object).Assembly;
    }

    /// <summary>
    /// Return the System.Type of the string
    /// </summary>
    /// <param name="typeName"></param>
    /// <returns></returns>
    public static Type GetTypeByAssembly(string typeName)
    {
        return Type.GetType(typeName) ?? GetUnityEngineAssembly().GetTypes().FirstOrDefault(t => t.Name == typeName);
    }

    /// <summary>
    /// Return all user types
    /// </summary>
    /// <param name="wantedType"></param>
    /// <returns></returns>
    public static IEnumerable<Type> GetAllUserTypesOf(Type wantedType)
    {
        return Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsSubclassOf(wantedType));
    }
}