﻿/*********************
* Copyright (c) 2014, Rudolf Chrispens.  All rights reserved.
* found on http://www.rudolf-chrispens.de/posts/
* Copyrights licensed under the GNU License.
* https://www.gnu.org/licenses/gpl-3.0
***********************/

using UnityEngine;
using System.Collections;

namespace RUnityScripts
{
	public class TestScript : MonoBehaviour 
	{
        public Transform MyTransform = null;
        public GameObject MyGameObject = null;
        public MonoBehaviour MyBehaviour = null;

        [HideIfNot("Test", "test")]
        public Light MyLight = null;
        public Rigidbody MyRigidbody = null;
        [HideIfNot("asf", 1)]
        public Component MyComponent = null;
        [HideIfNot("Test", "test")]
        public BoxCollider MyBoxCollider = null;

        public string Test = "test";
        public Vector2 asf = Vector2.zero;

        void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(Vector3.zero, 1.0f);
        }
	}
}